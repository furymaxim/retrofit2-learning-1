package com.furymaxim.retrofitmulti

import com.google.gson.annotations.SerializedName

class CreateUserList {

    @SerializedName("name")
    var name: String? = null
    @SerializedName("job")
    var job: String? = null
    @SerializedName("id")
    var id: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null
}