package com.furymaxim.retrofitmulti


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_simple_example.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.R.attr.resource
import android.R.attr.resource
import android.R.attr.data
import android.widget.Toast
import com.squareup.picasso.Picasso




class SimpleExampleFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_simple_example, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        getResourcesButton.setOnClickListener {
            if(scrollViewResources.visibility != View.GONE) requestResourcesText.text = ""

            getResourcesInfo()

            hideResourcesButton.visibility = View.VISIBLE
            scrollViewResources.visibility = View.VISIBLE
        }

        hideResourcesButton.setOnClickListener {
            scrollViewResources.visibility = View.GONE
            hideResourcesButton.visibility = View.GONE
        }

        postUserButton.setOnClickListener {
            if(requestUserName.text.isNotBlank() && requestUserJob.text.isNotBlank())
                    postNewUser(requestUserName.text.toString(), requestUserJob.text.toString())
        }

        getUserListButton.setOnClickListener {
            if(scrollViewUsers.visibility != View.GONE) requestUsersText.text = ""
            scrollViewResources.visibility = View.GONE
            hideUserListButton.visibility = View.GONE
            scrollViewUsers.visibility =  View.VISIBLE

            if(pageValue.text.toString() == "1" || pageValue.text.toString() =="2") {
                val page = pageValue.text.toString()
                getUserList(page)
                hideUserListButton.visibility = View.VISIBLE
            }
            else{
                Toast.makeText(context,"Доступна только страница 1 или 2",Toast.LENGTH_SHORT).show()
            }
        }

        hideUserListButton.setOnClickListener {
            scrollViewUsers.visibility = View.GONE
            hideUserListButton.visibility = View.GONE
        }

        postAdminButton.setOnClickListener {
            postCreateUserWithField("Maxim","admin")
        }

    }

    private fun postCreateUserWithField(name:String, job:String){
        NetworkService.getInstance()
            .getJSONApi()
            .doCreateUserWithField(name,job)
            .enqueue(object:Callback<User>{
                override fun onFailure(call: Call<User>, t: Throwable) {
                    call.cancel()
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    if(response.isSuccessful){


                        val user = response.body()

                        Toast.makeText(context, "Created a new user.\nInfo:\nid: ${user!!.id}\nname: ${user.name}\njob: ${user.job}\ncreated at: ${user.createdAt}",Toast.LENGTH_SHORT).show()

                    }
                }
            })
    }

    private fun getUserList(page: String){
        NetworkService.getInstance()
            .getJSONApi()
            .doGetUserList(page)
            .enqueue(object: Callback<UserList>{
                override fun onFailure(call: Call<UserList>, t: Throwable) {
                    call.cancel()
                }

                override fun onResponse(call: Call<UserList>, response: Response<UserList>) {
                    if(response.isSuccessful){

                        var displayResponse = ""

                        val userList = response.body()
                        val page = userList!!.page
                        val perPage = userList.perPage
                        val total = userList.total
                        val totalPages = userList.totalPages
                        val datumList = userList.data

                        var i = 0

                        displayResponse += " Page: $page;\nPer page: $perPage;\nTotal users: $total;\nTotal pages: $totalPages\n"

                        for(datum in datumList){
                            displayResponse += " id: ${datum.id}, first name: ${datum.first_name}, last name: ${datum.last_name}, avatar: ${datum.avatar};\n"
                            if(i == 1)
                                Picasso.get().load(datum.avatar).into(requestUsersImage)
                            i++
                        }

                        requestUsersText.text = displayResponse
                    }
                }
            })
    }


    private fun postNewUser(userName:String, userJob:String){
        NetworkService.getInstance()
            .getJSONApi()
            .createUser(User(userName,userJob))
            .enqueue(object: Callback<User>{
                override fun onFailure(call: Call<User>, t: Throwable) {
                    call.cancel()
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    if(response.isSuccessful){

                        val user = response.body()

                        Toast.makeText(context, "Created a new user.\nInfo:\nid: ${user!!.id}\nname: ${user.name}\njob: ${user.job}\ncreated at: ${user.createdAt}",Toast.LENGTH_SHORT).show()

                    }
                }
            })
    }


    private fun getResourcesInfo(){
        NetworkService.getInstance()
            .getJSONApi()
            .doGetListResources()
            .enqueue(object: Callback<MultipleResource>{

                override fun onFailure(call: Call<MultipleResource>, t: Throwable) {
                    call.cancel()
                }

                override fun onResponse(call: Call<MultipleResource>, response: Response<MultipleResource>) {
                   if(response.isSuccessful){
                       Log.i("myLogs", response.code().toString())

                       val resource = response.body()
                       val page = resource!!.page
                       val total = resource.total
                       val totalPages = resource.totalPages
                       val datumList = resource.data


                       var displayResponse = ""
                       displayResponse += " Page: $page;\nTotal resources: $total;\nTotal pages: $totalPages\n"


                       for(datum in datumList!!){
                           displayResponse += " id: ${datum.id}, name: ${datum.name}, year: ${datum.year}, pantone: ${datum.pantoneValue};\n"
                       }

                       requestResourcesText.text = displayResponse

                   }
                }
            })



    }


}
