package com.furymaxim.retrofitmulti

import com.google.gson.annotations.SerializedName



class User constructor(name: String, job: String){

    @SerializedName("name")
    var name: String? = null
    @SerializedName("job")
    var job: String? = null
    @SerializedName("id")
    var id: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null

    init {
        this.name = name
        this.job = job
    }
}